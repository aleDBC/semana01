JUnit

- @Test (posso tratar exce��es)
- @Ignore (testes que s�o ignorados)
- @BeforeClass (executa os teste antes dos @Test | Statico)
- @AfterClass (executa os teste depois de todos @Test | Statico)

- @Before (executa os teste antes de cada @Test)
- @After (executa os teste depois de cada @Test)
	- Para cada @test s�o, @Before e @After s�o executados

Test Suites
- Agrupa diversos tests cases (n�o precisa ter nada no corpo)
- @RunWith e @SuiteClass

Lifecycle Hooks:
(eles ajudam a n�o ter muitas repeti��es nos testes)
- @BeforeAll (precisa ser est�tico, caso @TestInstance n�o for PER_METHOD)
- @AfterAll
- @BeforeEach (cada teste)
- @AfterEach  (cada teste)


Eu consigo mudar o nome do teste se eu colocar na frente:
- @DisplayName

TestInfo e TestReporter me mostra informa��es dos testes.