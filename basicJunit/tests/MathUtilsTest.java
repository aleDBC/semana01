import org.junit.jupiter.api.*;
import org.junit.jupiter.api.condition.EnabledOnOs;
import org.junit.jupiter.api.condition.OS;

import static org.junit.jupiter.api.Assertions.*;

public class MathUtilsTest {

    MathUtils mathUtils;
    TestInfo testInfo;
    TestReporter testReporter;


    @BeforeAll
    static void beforeAllInit(){
        System.out.println("Isso roda antes de tudo");
    }

    @BeforeEach
    // @RepeatedTest(2) faz o teste se repetir
    void init(TestInfo testInfo, TestReporter testReporter){
        this.testInfo = testInfo;
        this.testReporter = testReporter;

        mathUtils = new MathUtils();
        testReporter.publishEntry("Rodando " + testInfo.getDisplayName() + " com as tags: " + testInfo.getTags());
    }

    @AfterEach
    void cleanup(){
        System.out.println("Linpani");
    }

    @Nested
    @DisplayName("Testes de add")
    class AddTest{

        @Test
        void testAddPositive(){
            assertEquals(2, mathUtils.add(1,1) , "Deve somar dois valores");
        }

        @Test
        void testAddNegative(){
            assertEquals(-2, mathUtils.add(-1,-1) , "Deve somar dois valores");
        }

    }


    @Test
    @Tag("Math")
    @DisplayName("Usar assertAll")
    void variosTestes(){
        assertAll(
                () -> assertEquals(4, mathUtils.add(2,2)),
                () -> assertEquals(0, mathUtils.sub(3,3)),
                () -> assertEquals(1, mathUtils.divide(5,5))
        );
    }

    @Test
    @Tag("Dividir ")
    @DisplayName("Teste para erro")
    void testDivide(){
        assertThrows(ArithmeticException.class, () -> mathUtils.divide(1,0), "Deve dar erro dividir por zero");
    }

    @Test
    @DisplayName("Teste para sucesso")
    @EnabledOnOs(OS.WINDOWS)
    void testDivideOk(){
        int esperado = 2;
        int atual = mathUtils.divide(10,5);
        assertEquals(esperado, atual , "Deve dividir dois valores");
    }

}
