<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
	<head>
		<title>Todo</title>
	</head>
	<body>
		<h1>Welcome ${name}</h1>
		<p>Sua lista de afazeres �:</p>
		<ol>
			<c:forEach items="${todos}" var="todo">
				<li>${todo.name} &nbsp; &nbsp; <a href="delete-todo.do?todo=${todo.name}" >Remover</a> </li>
			</c:forEach>	
		</ol>
		<form action="/todo.do" method="post">
			<input type="text" name="todo"/>
			<input type="submit" value="Add"/>
		</form>
	</body>
</html>