package com.webApp.todo;

import java.util.ArrayList;
import java.util.List;

public class TodoService {
	private static List<Todo> todos = new ArrayList<Todo>();
	
	static {
		todos.add(new Todo("Aprender Web App Development"));
		todos.add(new Todo("Aprender Spring"));
		todos.add(new Todo("Aprender Web Server SOAP"));
	}
	
	public List<Todo> retrieveTodos(){
		return todos;
	}
	
	public void addTodo(String todo) {
		todos.add(new Todo(todo));
	}
	
	public void deleteTodo(Todo todo) {
		todos.remove(todo);
	}
	
}
